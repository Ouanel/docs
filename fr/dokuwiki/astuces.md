# Astuces

## Supprimer une page wiki

Pour supprimer une page wiki vous devez supprimer l'intégralité de son contenu et sauvegarder la page.

## Créer une barre de navigation

Pour avoir une barre de navigation latérale, vous devez créer la page `sidebar`. Pour ce faire :

  * allez sur la page principale de votre site (`https://monsite.frama.wiki/`)
  * ajoutez `sidebar` à la fin de l'adresse (`https://monsite.frama.wiki/sidebar`)
  * cliquez sur **DokuWiki Editor**

    ![DokuWiki edtition menu](images/dokuwiki_edition_sidebar.png)

  * ajoutez ce que vous souhaitez mettre dans la barre de navigation - par exemple :
    ```
    * [[https://framasoft.org|Framasoft]]
    * [[https://docs.framasoft.org/fr/|Documentation Framasoft]]
    * [[https://contributopia.org/|Contributopia]]
    ```
  * cliquez sur **Enregistrer**

  **Avant sidebar** :

  ![page avant sidebar](images/dokuwiki_page_avant_sidebar.png)

  **Après sidebar** :

  ![page après sidebar](images/dokuwiki_page_apres_sidebar.png)

## Modifier le titre et la description

Pour modifier le titre et/ou la description de son wiki, il faut :

* cliquer sur **Administrer**
* cliquer sur **Paramètres de configuration**
* modifier **Titre du wiki (nom du wiki)** ou **Descriptif du site (si le modèle supporte cette fonctionnalité)**
* cliquer sur **Enregistrer** (tout en bas de la page)
