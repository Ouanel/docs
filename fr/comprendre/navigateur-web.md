# Comprendre les pages et navigateur web (avec TopChef)

## Imaginons

On a tendance à croire qu'une page web, c'est comme la page d'un livre ou d'un magazine : une fois imprimée, elle est pareilles pour tout le monde. Or, c'est rarement le cas.

Une page web, c’est un plat, fait selon une recette de cuisine :

> Mettez un titre en gros, en gris et en gras.

> Réduisez l’image afin qu’elle fasse un quart de la colonne d’affichage, réservez.

> Placez le texte, agrémenté d’une jolie police, aligné à gauche, puis l’image à droite.

> Servez chaud.

**Derrière chaque page web, il y a un code, une recette de cuisine.**

## Le navigateur web c’est le cuisinier

Mon navigateur web (Firefox, Chrome, Safari, Internet Explorer ou Edge…), c'est le cuisinier qui va lire la recette et mitonner le plat.

Il va télécharger les ingrédients, et suivre la recette. T’as déjà vu quand on donne la même recette avec les mêmes ingrédients à 4 cuisiniers différents ? Ben ouais, c’est comme dans Top Chef, ça fait 4 plats qui sont pas vraiment pareils.

**Chaque navigateur web interprète la recette à sa manière.**

## Nous n'avons pas les mêmes ustensiles de cuisine

Les plats seront d'autant plus différents si les assiettes ne sont pas de la même taille (genre l’écran de mon téléphone et celui de mon ordi…).

Chaque cuisinier n'utilise pas les mêmes ustensiles : les navigateurs web ne proposent pas tous les mêmes options, et certains sont personnalisables (on peut y ajouter des extensions, qui sont autant d'ustensiles de cuisines supplémentaires).

Enfin, **mon cuisinier (mon navigateur web) sait des choses sur moi : la recette de cuisine va s'adapter selon mon profil de gourmet**... Mes préférences (opions de navigation, extensions), chez quel cuisinier j'ai été avant (comptes connectés, _referer_), la taille de mon assiette et la cuisine que j'utilise (mon écran, mon système d'exploitation sont donnés par le _user agent_)... toutes ces informations peuvent induire des changements dans la recette de cuisine.



**La page web que je lis aura peu de chance d’avoir la même gueule pour moi et la personne à qui je la ferai passer ;)**
