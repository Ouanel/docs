# Composants Bootstrap (version 3.3)

Il est possible d’utiliser directement le code html de
[tous les composants Bootstrap](https://getbootstrap.com/docs/3.3/components/)
ainsi que [les classes CSS](https://getbootstrap.com/docs/3.3/css/).

Mais pour « simplifier » la syntaxe, le thème prévoit certains raccourcis (shortcodes)
qui permettent de garder la syntaxe markdown entre les balises (ce que ne permet pas le html seul).

## Accordion

![](images/accordion.gif)

``` markdown
[g-accordion id=accordion1 name=accordion1]
[g-accordion-item id=accordion_item1 header_id=accordion_header1 title="Open me"]

Anim pariatur cliche **reprehenderit**, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.

[/g-accordion-item]
[g-accordion-item id=accordion_item2 header_id=accordion_header2 title="Open me too"]

Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.

[/g-accordion-item]
[/g-accordion]
```

## Alertes

<div class="alert alert-warning " role="alert">
    <p class="h2">Oh snap! You got an error!</p>
    <p>Change this and that and try again. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum.</p>
    <p><button class="btn btn-danger">Take this action</button>
    <button class="btn btn-default">Or do this</button></p>
</div>

``` markdown
[g-alert name=alert1 type=warning]
# Oh snap! You got an error!

Change this and that and try again. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum.

[Take this action](#) {.btn .btn-danger}
[Or do this](#) {.btn .btn-default}
[/g-alert]
```

## Carousel

![](images/carousel.png)

``` markdown
[g-carousel id="carousel1" name="carousel1" images_path="04.plugins/plugin-160204-gravstrap/carousel-160205"]

[g-carousel-item image="image1.jpg"]
**Duis mollis**, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.
[/g-carousel-item]

[g-carousel-item image="image2.jpg"]
Cras mattis consectetur purus sit amet fermentum.
[/g-carousel-item]

[g-carousel-item image="image3.jpg"]
Cras mattis consectetur purus sit amet fermentum.
[/g-carousel-item]
[/g-carousel]
```

## Collapse

<button class="btn btn-default " role="button" data-toggle="collapse" data-target="#collapse1" aria-expanded="false" aria-controls="collapse">
  Collapse
</button>
<div class="collapse" id="collapse1">
    <div class="well ">
        <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</p>
    </div>
</div>

``` markdown
[g-collapse id="collapse1" name=collapse1 button_label="Collapse" attributes="aria-expanded:false,aria-controls:collapse"]
[g-well name=well2]
Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.
[/g-well]
[/g-collapse]
```

## Dropdown

<div class="dropdown">
    <button class="btn btn-default dropdown-toggle " type="button" data-toggle="dropdown" aria-expanded="false">
        Dropdown
        <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" style="padding:0">
        <li><a href="https://frama.site"><i class="fa fa-cog "></i>Products</a></li>
        <li><a href="https://frama.site"><i class="fa fa-gavel "></i>Contacts</a></li>
    </ul>
</div>



``` markdown
[g-dropdown name=dropdown1 label="Dropdown"]
    [g-dropdown-item icon_type="fontawesome" ]
        [g-link url="http://example.com" menu="Products" icon="cog"][/g-link]
        [g-link url="http://example.com" menu="Contacts" icon="gavel"][/g-link]
    [/g-dropdown-item]
[/g-dropdown]
```

## Jumbotron

![](images/jumbotron.png)

``` markdown
[g-jumbotron name="jumbotron1" image="image1.jpg"]
# Gravstrap theme

Gravstrap Theme helps you to start a new Grav CMS site with Bootstrap support and several ready to use modules. It is also perfect to start a new Bootstrap custom theme, to create your unique design.

[/g-jumbotron]
```

## List group

<div class="list-group">
    <a href="https://frama.site" class="list-group-item list-group-item- active foo" rel="bar">
        <span class="badge">12</span>
        <p>Cras mattis consectetur purus sit amet fermentum.</p>
    </a>
    <a href="https://frama.site" class="list-group-item list-group-item- ">
        <span class="badge">15</span>
        <p>Cras mattis consectetur purus sit amet fermentum.</p>
    </a>
    <a href="https://frama.site" class="list-group-item list-group-item- disabled ">
        <span class="badge"></span>
        <p>Cras mattis consectetur purus sit amet fermentum.</p>
    </a>
    <a href="https://frama.site" class="list-group-item list-group-item- ">
        <span class="badge">2</span>
        <p>Cras mattis consectetur purus sit amet fermentum.</p>
    </a>
</div>

``` markdown
[g-listgroup name=listgroup1]

[g-listgroup-item badge=12 active=true]
Cras mattis consectetur purus sit amet fermentum.
[/g-listgroup-item]

[g-listgroup-item badge=15]
Cras mattis consectetur purus sit amet fermentum.
[/g-listgroup-item]

[g-listgroup-item disabled=true]
Cras mattis consectetur purus sit amet fermentum.
[/g-listgroup-item]

[g-listgroup-item badge=2]
Cras mattis consectetur purus sit amet fermentum.
[/g-listgroup-item]
[/g-listgroup]
```

## Modal

![](images/modal.gif)

``` markdown
[g-button button_label="Launch demo modal" button_type="primary" button_attributes="data-toggle:modal,data-target:#myModal"][/g-button]

[g-modal id="myModal" name="modal" title="Awesome"]
Cras **mattis** consectetur purus sit amet fermentum.

[g-modal-buttons]
[g-button button_type="primary" button_label="Click Me" remove=true][/g-button]
[g-button button_label="Close Me" button_attributes="data-dismiss:modal"][/g-button]
[/g-modal-buttons]

[/g-modal]
```

## Navbar

![](images/navbar.png)

``` markdown
[g-navbar id="navbar3" name="navbar3" centering=none brand_text="Brand"]
    [g-navbar-menu name=menu00 alignment="center" submenu="internal,components"][/g-navbar-menu]
    [g-navbar-menu name=menu01 icon_type="fontawesome" alignment="right" attributes="class: my-class,rel:my-rel"]
        [g-link url="https://framasphere.org" icon_type="fontawesome" icon="asterisk"][/g-link]
        [g-link url="https://framapiaf.org" icon="retweet"][/g-link]
        [g-link url="https://framagit.org" icon="git"][/g-link]
    [/g-navbar-menu]
[/g-navbar]
```

## Page header

<p class="h1">Titre <small>Sous-titre</small></p>

``` markdown
[g-pageheader title="Titre" subtitle="Sous-titre"][/g-pageheader]
```

## Panel

<div class="panel panel-default">
    <div class="panel-heading">Title</div>
    <div class="panel-body">
      <p>Anim pariatur cliche <strong>reprehenderit</strong>, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.</p>
    </div>
</div>

``` markdown
[g-panel heading_title="Title"]
Anim pariatur cliche **reprehenderit**, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
[/g-panel]
```

## Progressbar

<div class="progress-label right">Php</div>
<div class="progress">
    <div class="progress-bar progress-bar-success " role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
        <span class="sr-only">80% Complete</span>
    </div>
</div>

``` markdown
[g-progressbar label="Php" type="success" value="80" min="0" max="100"][/g-progressbar]
```

## Splitbutton

<div class="btn-group">
  <button type="button" class="btn btn-default ">Splitbutton</button>
  <button type="button" class="btn btn-default dropdown-toggle " data-toggle="dropdown">
    <span class="caret"></span>
    <span class="sr-only">Splitbutton</span>
  </button>
  <ul class="dropdown-menu" style="padding:0">
    <li><a href="https://framasphere.org"><i class="fa fa-asterisk "></i>
Diaspora</a></li>
    <li><a href="https://framapiaf.org"><i class="fa fa-retweet "></i>
Mastodon</a></li>
  </ul>
</div>



``` markdown
[g-splitbutton name=splitbutton1 label="Splitbutton" type=default]
    [g-splitbutton-item icon_type="fontawesome" ]
        [g-link url="https://framasphere.org" menu="Diaspora" icon="asterisk"][/g-link]
        [g-link url="https://framapiaf.org" menu="Mastodon" icon="retweet"][/g-link]
    [/g-splitbutton-item]
[/g-splitbutton]
```

## Tab

![](images/tab.gif)


``` markdown
[g-tab name=tab1 attributes="class:myclass,rel:myrel"]

[g-tab-item name="tab_item1" attributes="class:myclass,rel:myrel"]
**Cras mattis** consectetur purus sit amet fermentum.
[/g-tab-item]

[g-tab-item name="tab_item2"]
Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.
[/g-tab-item]
[/g-tab]
```

## Thumbnail

Avec des liens :

<div class="row">
    <div class="col-md-4">
        <a href="https://frama.site" class="thumbnail">
            <img src="images/image1.jpg">
        </a>
    </div>
    <div class="col-md-4">
        <a href="https://frama.site" class="thumbnail">
            <img src="images/image2.jpg">
        </a>
    </div>
    <div class="col-md-4">
        <a href="https://frama.site" class="thumbnail">
            <img src="images/image3.jpg">
        </a>
    </div>
</div>

``` markdown
[g-thumbnail name=thumbnail1]
[g-thumbnail-item image="image1.jpg" url="https://frama.site"][/g-thumbnail-item]
[g-thumbnail-item image="image2.jpg" url="https://frama.site"][/g-thumbnail-item]
[g-thumbnail-item image="image3.jpg" url="https://frama.site"][/g-thumbnail-item]
[/g-thumbnail]
```

Avec du contenu :

<div class="row">
    <div class="col-md-6">
        <div class="thumbnail">
            <img src="images/image1.jpg">
            <div class="caption">
                <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</p>
<p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</p>
<p><a href="/gravstrap-components-for-grav-cms-application/bootstrap-components-as-shortcodes-for-grav-cms" class="btn btn-default">Take this action</a></p>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="thumbnail">
            <img src="images/image2.jpg">
            <div class="caption">
                <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</p>
<p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</p>
<p><a href="/gravstrap-components-for-grav-cms-application/bootstrap-components-as-shortcodes-for-grav-cms" class="btn btn-default">Take this action</a></p>
            </div>
        </div>
    </div>
</div>

``` markdown
[g-thumbnail name=thumbnail2]
[g-thumbnail-item image="image1.jpg" class="col-md-6"]
Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.

Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.

[Take this action](#) {.btn .btn-default}
[/g-thumbnail-item]

[g-thumbnail-item image="image2.jpg" class="col-md-6"]
Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.

Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.

[Take this action](#) {.btn .btn-default}
[/g-thumbnail-item]

[/g-thumbnail]
```

## Well

<p class="well ">Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</p>

``` markdown
[g-well name=well1]
Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.
[/g-well]
```
