<p class="alert alert-warning">La documentation est en cours de rédaction.<br>
Si vous souhaitez apporter des compléments, vous pouvez contribuer
sur <a href="https://framagit.org/framasoft/docs">le dépôt git</a>,
soit en ouvrant une « issue » décrivant les modifications,
soit en proposant une « merge request ».</p>

# Prise en main

## Gestion des images

### Images globales

Le dossier `/images` contient l’ensemble des images communes à tout le site.

Il sert de référence pour les modules et notament le module <code>images-collage</code>
ce qui permet d’éviter d’avoir à saisir l’ensemble du chemin d’accès aux images ;
le nom du fichier seul suffit.

Dans d’autres contextes il faudra utiliser la syntaxe markdown
<pre><code>![](/images/une-image.jpg)</code></pre>

L’adresse de l’image sera du genre :

    https://demo.frama.site/user/pages/images/une-image.jpg

<p class="alert alert-info">
Pour uploader une image, il suffit de la glisser-déposer
(ou de cliquer pour ouvrir l’explorateur de fichiers) avec la souris
dans le cadre gris « Medias de la page ».
</p>

### Images relatives

Pour les pages et articles, il est possible d’ajouter des images
directement liées au contenu de l’article.
Pour pouvoir uploader les fichiers, il faut que l’article ou la page
ait déjà été engegistré (sans nécéssairement le publier).

La première image ajoutée sert systématiqument d’image « à la une »
pour les articles de blog.

Pour ajouter une image dans le corps du texte, passez simplement la souris sur l’image et
cliquez sur le bouton <i class="fa fa-plus-circle"></i> à sa droite.
Un code markdown de ce genre devrait être inséré :
<pre><code>![](une-image.jpg)</code></pre>

Quand l’article sera publié l’adresse de l’image sera du genre :

    https://demo.frama.site/user/pages/01.page/article/une-image.jpg

### Modifier l’image d’entête

Voici ci-dessous une vidéo illustrant comment, par exemple, modifier l’image d’entête
sur une page avec une « navbar interne ».<br>
L’entête est défini dans le module « header ».<br>
L’image par défaut `bg_home.jpg` est une « image globale ».
qui se trouve donc dans le dossier `/images`.

<div class="embed-responsive embed-responsive-4by3">
  <video controls="controls" preload="none" muted
    poster="images/image-entete.jpg"
    class="embed-responsive-item"
    width="640" height="480">
    <source src="images/image-entete.mp4" type="video/mp4">
  </video>
</div>

### Redimensionner une image

Grav permet de redimensionner les images au cas par cas,
il suffit pour ça d’ajouter `?cropResize=<largeur>,<hauteur>` après le nom du fichier :
<pre><code>![](une-image.jpg?cropResize=640,360)</code></pre>

Mais vous pouvez aussi bien garder l’image originale et lui ajouter la classe CSS `img-responsive`
pour que sa taille s’adapte selon la résolution d’écran de vos visiteurs :
<pre><code>![](une-image.jpg) {.img-responsive}</code></pre>

## Utilisation des templates

### En cours…

## Personnalisation du site

### La barre de navigation et le pied de page
Excepté pour les `page_navbar_interne`, la barre de navigation et
le pied de page sont définis dans la page `common`.

<p class="alert alert-danger">Lorsque vous modifiez cette page, il est important de bien faire attention
que les shortcodes soient correctement fermés.<br>
S’il manque une balise de fermeture, le site ainsi que l’espace admin
riquent d’être complètement cassés.</p>

Le menu principal contient la liste des pages de premier niveau.

Le titre « Framasite » est défini dans l’attribut `brand_text`.
Les icônes de réseau sociaux en haut à droite correspondent aux shortcodes `[g-link]`
dont la syntaxe est décrite dans les [composants de base](/fr/grav/composants-de-base.html#ic%C3%B4nes-et-liens).

Voici une vidéo illustrant comment personnaliser tout ça :

<div class="embed-responsive embed-responsive-4by3">
  <video controls="controls" preload="none" muted
    poster="images/gravstrap-navbar-footer.jpg"
    class="embed-responsive-item"
    width="640" height="480">
    <source src="images/gravstrap-navbar-footer.mp4" type="video/mp4">
  </video>
</div>

### Ajouter des styles

Pour personnaliser l’apparence du site, il faut utiliser une feuille de style CSS.

Rendez-vous dans `Plugins > CustomCSS` et collez dans le champ `Inline CSS` votre feuille de style.

Illustration en vidéo avec un thème noir/orange dont
[la feuille de style est celle-ci](images/grav-dark.css).

<div class="embed-responsive embed-responsive-4by3">
  <video controls="controls" preload="none" muted
    poster="images/grav-customcss.jpg"
    class="embed-responsive-item"
    width="640" height="480">
    <source src="images/grav-customcss.mp4" type="video/mp4">
  </video>
</div>

Un jeu de thème vous est également proposé en dessous de la zone de texte.<br>
En cliquant sur le bouton <i class="fa fa-plus-circle" aria-hidden="true"></i>
<span class="sr-only">Utiliser</span>, le champ sera rempli avec la feuille de style adéquate.

### Ajouter des formules mathématiques

Vous avez la possibilité d’insérer des formules mathématiques écrites en LATEX.
Il faut pour cela les encadrer de ce code HTML :

    <p class="mathjax mathjax--block">…</p>

Exemples :

    <p class="mathjax mathjax--block">
    $$
    \begin{align}
      g_{\mu \nu} & = [S1] \times \operatorname{diag}(-1,+1,+1,+1) \\[6pt]
      {R^\mu}_{\alpha \beta \gamma} & = [S2] \times (\Gamma^\mu_{\alpha \gamma,\beta}-\Gamma^\mu_{\alpha \beta,\gamma}+\Gamma^\mu_{\sigma \beta}\Gamma^\sigma_{\gamma \alpha}-\Gamma^\mu_{\sigma \gamma}\Gamma^\sigma_{\beta \alpha}) \\[6pt]
      G_{\mu \nu} & = [S3] \times {8 \pi G \over c^4} T_{\mu \nu}
    \end{align}
    $$
    </p>

![](images/mathjax1.png)

    <p class="mathjax mathjax--block">
    $$
    R_{\mu \nu}=[S2]\times [S3] \times {R^\alpha}_{\mu\alpha\nu}
    $$
    </p>

![](images/mathjax2.png)

## Administration

### Gestion des utilisateurs

Vous pouvez ajouter des utilisateurs à votre site : soit en tant que **simple** utilisateur avec des droits limités, ou en tant qu'**admin**.

Pour ce faire, vous devez :
 * vous connecter sur [frama.site](https://frama.site/)
 * cliquer sur le bouton **Paramètres de ce site** dans le cadre du site que vous souhaitez modifier
 * cliquer sur le bouton **Ajouter un utilisateur** sous la liste des utilisateurs déjà créés
 * entrer :
  * son adresse mail
  * son identifiant pour le site
  * son mot de passe
  * son nom complet (facultatif : sera **affiché publiquement** à la place de votre identifiant sur les pages de votre site, comme par exemple vos posts de blog ou commentaires.)
  * laisser cochée la case **Admin** pour en faire un administrateur du site (qui pourra modifier le site et sa structure) ou décocher la case pour en faire un simple utilisateur
