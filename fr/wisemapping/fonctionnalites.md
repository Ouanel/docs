# Fonctionnalités

## Publier une carte

Pour publier une carte mentale vous devez :

* cliquer sur l'icône **Publier** : ![icône Publier](images/mindmap_publier.png)
* cocher **Activer le partage**

Si vous souhaitez *embarquer* la carte sur un site, vous devez alors copier le code du style :

```
<iframe style="width:600px;height:400px;border: 1px
solid black" src="https://framindmap.org/c/maps/CHIFFRES/embed?zoom=1"> </iframe>
```
Sinon, vous pouvez cliquer sur l'onglet **URL Publique** et copier l'url de votre carte pour la donner aux personnes souhaitées.

N'oubliez pas de cliquer sur **Accepter** pour confirmer la publication de votre carte.
