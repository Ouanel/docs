# Création et configuration d'un compte Framagit

## Ce que nous allons apprendre

*  Création de son compte
*  Personnalisation de son profil
*  Navigation sur le Framagit

## Procédons par étapes

La première chose à faire pour utiliser Framagit est de créer son compte.

### 1. Création du compte

Pour créer un compte Framagit, rendez-vous à cette adresse : [https://framagit.org/users/sign\_in?redirect\_to\_referer=yes](https://framagit.org/users/sign_in?redirect_to_referer=yes)

puis cliquer en haut à droite sur le bouton **Register**

![](images/Framagit-Register.png)

Renseigner les champs _Name, Username, Email, Password_ puis cliquer en dessous sur le bouton bleu **Register**. Un message vous sera envoyé à l'adresse mail que vous avez renseignée, avec un lien pour confirmer ma création de votre compte : « Confirm your account ».

![](images/Email-1.png)

Suivre le lien pour valider le compte.

Faire très attention que pour l'opération de « _Sign in_ » (c'est-à-dire la connexion sur le site Framagit avec votre identification), il faut choisir « Standard » parmi les trois options en haut à droite (_LDAP, Standard, Register_).

![](images/Framagit-login.png)

Arrivé à ce stade il vous faut demander à l'un des administrateurs du projet l'autorisation d'y accéder, en donnant bien entendu l'identifiant de votre compte.

Lorsque ceci sera fait, vous pourrez passer à l'étape suivante : démarrer les opérations Git sur votre machine locale, et ajouter votre clé SSH (ce que nous ferons dans la partie suivante) dans votre compte Framagit.

### 2. Modification du profil

Lors d' une prochaine connexion à votre compte vous pouvez renseigner vos informations personnelles, en cliquant sur la petite flèche en haut à droite de la fenêtre 

![](images/Framagit-Profile.png) 

puis sur l'option « Profile Settings » dans le menu qui s'ouvre ou utiliser ce lien direct [https://framagit.org/profile](https://framagit.org/profile) :

![](images/Profile-Settings-1.png)

La fenêtre se remplit avec votre profil actuel, et vous pourrez renseigner ou modifier tous les champs que vous souhaitez, y compris votre avatar (la petite image qui vous suivra partout)

![](images/Profile-settings-2.png)

Puis cliquer tout en bas sur le bouton vert **Update profile settings**.

### 3. Navigation sur Framagit

Avant de quitter le site Framagit, voici comment faire pour y naviguer. Cliquer sur les trois petites barres en haut à gauche de la fenêtre. Un menu s'ouvre sur fond noir, et vous pouvez choisir la section qui vous intéresse.

![](images/Navigation-1.png) 

Ici nous allons choisir « Projects »

![](images/Navigation-2.png)

La fenêtre Framagit affiche maintenant les différents projets auxquels vous avez l'accès, et nous choisissons ici « Emmabuntus/Communication »

![](images/Navigation-3.png)

C'est donc la page d'accueil du projet Communications, qui nous informe des dernières activités en date, et propose en haut un certain nombre de sections : _Project, Activity, Repository_ etc … que vous pouvez explorer à votre guise. Veuillez aussi noter que la section « _Issues_ » sera de grande utilité plus tard pour communiquer avec le reste du collectif.

----

## Faisons le point

Maintenant que votre compte est créé, passons à l'[installation de votre poste de travail](3-installation-poste.html).
