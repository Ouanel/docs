# Identification
_____

Vous pouvez vous identifier dans votre équipe à partir de l'adresse web `https://domain.com/teamname`.

## Méthodes pour s'identifier

Il existe plusieurs options pour s'identifier dans votre équipe selon la manière dont votre administrateur système a configuré votre serveur.

#### Identification par adresse de courriel ou nom d'utilisateur

Quand vous êtes autorisé par votre administrateur système, vous pouvez vous identifier avec le nom d'utilisateur ou l'adresse de courriel utilisée pour la création du compte.

Si vous avez oublié votre mot de passe, vous pouvez le réinitialiser en cliquant sur l'onglet **J'ai oublié mon mot de passe** sur la page d'identification, ou en contactant votre administrateur système pour obtenir de l'aide afin de réinitialiser votre mot de passe.

#### GitLab Single-Sign-On (SSO)

Une fois autorisé par votre administrateur système, vous pouvez vous identifier avec votre compte GitLab en utilisant l'identification en un clic. GitLab SSO vous permet de créer des équipes, créer des comptes dans des équipes, et de vous identifier dans des équipes avec un nom d'utilisateur, une adresse de courriel et un mot de passe uniques qui marchent pour tout le serveur.

## Changer d'équipe

Quand vous avez utilisé la même adresse de courriel pour vous identifier et vous enregistrer dans plusieurs équipes, vous pouvez passer entre ces différentes équipes en utilisant le **Menu Principal**, accessible en cliquant sur les trois points dans le bandeau supérieur de chaque équipe sur le serveur. Par défaut, le système garde en mémoire dans quelles équipes vous vous êtes identifié les 30 derniers jours.

## Se déconnecter

Vous pouvez vous déconnecter depuis le **Menu Principal**, accessible en cliquant sur les trois points dans le bandeau supérieur, sur le côté gauche de l'écran. Cliquer sur «&nbsp;Déconnexion&nbsp;» vous déconnecte de toutes les équipes où vous étiez identifié et ouvertes dans votre navigateur.

## Configuration iOS

Vos équipes Mattermost sont accessibles sur les terminaux iOS en téléchargeant l'application Mattermost App.

1. Ouvrez l'App Store sur votre appareil Apple avec iOS 9.0 ou plus.

2. Cherchez « Mattermost » et cliquez sur **OBTENIR** pour télécharger gratuitement l'application.

3. Ouvrez Mattermost depuis votre écran d'accueil et renseignez-y votre équipe ainsi que vos identifiants de compte pour vous connecter :

    1. Entrez l'URL de l'équipe : c'est une partie de l'adresse web de votre équipe sur le domaine. Vous pouvez obtenir l'URL de l'équipe en demandant à votre administrateur système ou en regardant dans la barre d'adresse dans un navigateur internet avec Mattermost ouvert. Elle se présente sous la forme `https://domain.com/teamurl/`.

    2. S'identifier dans Mattermost : il s'agit de vos identifiants de compte comme décrit dans l'une des méthodes ci-dessus.


