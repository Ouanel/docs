# Couleurs du thème

_____

Les couleurs de l'interface utilisateur de Mattermost sont personnalisables dans **Paramètres du compte** > **Affichage** > **Thème**. Il est possible d'importer son propre thème depuis Slack, de personnaliser les couleurs soi-même, ou de choisir parmi les quatre thèmes par défaut conçus par l'équipe Mattermost.

## Thèmes standard

Sélectionnez **Couleurs du thème** pour choisir parmi les quatre thèmes par défaut conçus par l'équipe Mattermost. Pour personnaliser l'un d'eux, cliquez dessus puis sur **Personnaliser** pour le charger dans le sélecteur de couleurs.

## Importer des Thèmes depuis Slack


Pour importer un thème, allez dans le menu **Préférences > Thème de barre latérale** depuis Slack, ouvrez l'option de thème personnalisable, copiez le thème *color vector* et collez-le dans la zone de texte *Contribution thème Slack* de Mattermost. Les paramètres de thème qui ne sont pas personnalisables dans Slack seront les paramètres par défaut de Mattermost.


## Thèmes personnalisés

Vérifiez l'aperçu en temps réel des vos couleurs de thème personnalisé et cliquez sur **Sauvegarder** pour confirmer les modifications. Annulez vos modifications en sortant du module de paramétrage et en cliquant sur **Oui, Annuler** 

#### Styles du menu latéral

- **Barre latérale BG :** Couleurs d'arrière-plan de la fenêtre des canaux, et les barres latérales de paramètres du Compte et du Groupe. 

- **Barre latérale Texte :** Couleur du texte des canaux diffusés dans la fenêtre canaux, et onglets dans la barre latérale des paramètres du compte et du groupe.

- **Barre latérale Entête BG :** Couleur d'arrière-plan de l'entête en haut de la fenêtre des Canaux et d'entêtes des différents modules.

- **Barre latérale Entête du texte :** Couleur du texte de l'entête en haut de la fenêtre des Canaux et d'entête de tous les modules.

- **Barre latérale Texte Non Lu :** Couleur du texte des canaux non lus dans la fenêtre Canaux.

- **Barre latérale texte survolé BG :** Couleur de fond des noms de canaux et des onglets de paramètres lorsque vous les survolez

- **Barre latérale bordure de texte actif :** Couleur du marqueur rectangulaire à gauche du panneau des Canaux ou de la barre latérale des Paramètres indiquant le canal ou l'onglet actif.

- **Barre latérale couleur du texte actif :** Couleur du texte du canal ou de l'onglet actif dans le panneau des Canaux ou dans la barre latérale des Paramètres.

- **Indicateur de connexion :** Couleur de l'indicateur de connexion apparaissant près des noms des membres de l'équipe dans la liste des messages privés.

- **Indicateur d'absence.** Couleur de l'indicateur d'absence apparaissant à côté des noms des membres du groupe dans la liste des Messages Directs lorsqu'ils sont inactifs depuis 5 minutes. 

- **Diamant de mention BG :** Couleur de fond du diamant indiquant les mentions non lues, qui apparaît à droite du nom du canal. C'est aussi la couleur de fond pour l'indicateur des « Messages non lus en dessous/ au dessus » apparaissant en haut ou en bas du panneau des canaux sur les plus petites fenêtres de navigation.

- **Texte du Diamant de Mention :** Couleur du texte du diamant de mention indiquant le nombre de mentions non-lues. C'est aussi la couleur de l'indicateur des "messages non lus en dessous/au dessus".

#### Styles du Canal Central

- **Canal Central BG :** Couleur du panneau central, RHS, et du fond des modales.

- **Texte du Canal Central :** Couleur de tous les textes (à l'exception des mentions, liens, hashtags, et blocs de code) dans le panneau central, RHS et les modales.

- **Séparateur des nouveaux messages :** Le séparateur des nouveaux messages apparaît en-dessous du dernier message lu quand vous cliquez sur un canal avec des messages non lus.

- **Soulignage de mention BG :** Couleur de soulignage sous vos mots qui déclenchent des mentions dans le panneau central et RHS.

- **Texte de soulignage des mentions :** Couleur du texte des mots qui déclenchent des mentions dans le panneau central et RHS.

- **Thème du code :** Couleur de fond et de syntaxe pour tous les blocs de code.

#### Style des liens et des boutons

- **Couleur des liens :** Couleur du texte de tous les liens, hashtags, mentions des coéquipiers, et boutons de basse priorité.

- **Fond des boutons :** Couleur du fond rectangulaire de tous les boutons de haute priorité.

- **Texte des boutons :** Couleur du texte sur le fond rectangulaire de tous les boutons de haute priorité.

#### Exemples de thème personnalisé


Personnalisez les couleurs de votre thème et partagez-les en copiant-collant les vecteurs de thèmes dans la zone de texte. Voir les exemples de thèmes et leurs vecteurs correspondants ci-dessous.

![theme2](../../images/theme2.PNG)


```
#f2f0ec,#2e2e2e,#e8e6df,#424242,#515151,#e0e0e0,#66cccc,#594545,#52adad,#d4b579,#66cccc,#ffffff,#ffffff,#444444,#f2777a,#3dadad,#3dadad,#ffffff,#66cccc,#ffffff,github
```


___

![theme3](../../images/theme3.PNG)


```
#262626,#ffffff,#363636,#ffffff,#cccccc,#525252,#7e9949,#ffffff,#99cb3f,#b8b884,#7e9949,#ffffff,#ffffff,#444444,#90ad58,#54850c,#90ad58,#ffffff,#90ad58,#ffffff,monokai
```


___

![theme1](../../images/theme1.PNG)


```
#4f2f4c,#ffffff,#452842,#ffffff,#e5e5e5,#452842,#a65ea0,#ffffff,#52adad,#d4b579,#f2777a,#ffffff,#ffffff,#444444,#f2777a,#f2777a,#f2777a,#ffffff,#e08d8f,#ffffff,solarized_dark
```


___

![theme4](../../images/theme4.PNG)


```
#de718e,#ffffff,#de6785,#ffffff,#ffffff,#cc6983,#43e8d4,#ffffff,#88e0e5,#ccdb91,#55a3a8,#ffffff,#ffffff,#444444,#55a3a8,#55a3a8,#55a3a8,#ffffff,#55a3a8,#ffffff,solarized_light
```


___



