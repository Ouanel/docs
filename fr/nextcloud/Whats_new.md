Les nouveautés de Nextcloud 12.0.3
===========================================

1. Un moyen plus simple de sélectionner une nouvelle application

    ![](images/apps_menu.png)

2. Nouveau menu Contacts pour communiquer avec vos collègues ou amis plus facilement

    ![](images/contacts_menu.png)

3. Un menu de contact qui s'affiche par dessus les avatars, où que ce soit

    ![](images/contacts_popup.png)

4. La possibilité d'envoyer plusieurs liens de partage uniques, chacun avec ses propres paramètres, en saisissant les adresses mails (le destinataire recevra un e-mail)

    ![](images/multi_sharing.png)

5. De nombreuses autres améliorations et de nouvelles applications telles que le partage d'écran dans les appels vidéo, la nouvelle application Circles pour définir des groupes d'utilisateurs, les notifications *push*, les notifications relatives aux modifications des fichiers même lorsqu'ils sont partagés sur un autre serveur, l'annulation de la suppression de fichiers d'un dossier partagé, même si la suppression a été effectuée par un destinataire, le partage direct vers les réseaux sociaux et bien plus encore.
