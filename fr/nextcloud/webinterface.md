L'interface web de Nextcloud
===========================


Vous pouvez vous connecter à votre serveur Nextcloud via un navigateur web, il suffit de le faire pointer sur le serveur Nextcloud et de saisir votre nom d'utilisateur et votre mot de passe.

Les navigateurs web pris en charge sont :

-   **Mozilla Firefox** 14+
-   **Google Chrome/Chromium** 18+
-   **Safari** 7+
-   **Internet Explorer** 11+
-   **Microsoft Edge**

    ![](images/login_page.png)

Navigation sur l'interface principale
----------------------------------


Par défaut, l'interface web de Nextcloud s'ouvre sur la page de vos fichiers. Vous pouvez ajouter, retirer, partager des fichiers et appliquer des modifications selon les droits d'accès dont vous disposez (les vôtres si vous administrez le serveur) ou ceux qui vous ont été attribués par l'administrateur du serveur).

![](images/files_page.png)

L'interface utilisateur Nextcloud contient les champs et fonctions suivants :

-   **Le menu de sélection des applications** (1) : Situé dans le coin supérieur gauche, vous y trouverez l'ensemble des applications disponibles sur votre instance Nextcloud. Cliquer sur l'icône d'une application vous redirigera vers cette application.

-   **La zone d'information de l'application** (2) : Situé dans la barre latérale à gauche, ce menu fournit différentes tâches et filtres relatifs à l'application sélectionée. Ainsi, quand vous utilisez l'application Fichiers, vous  pouvez utiliser différents filtres pour trouver rapidement certains fichiers et notamment les fichiers qui ont été partagés avec vous ou ceux que vous avez partagés. D'autres éléments seront affichés pour d'autres applications.

-   **La vue de l'application** (3) : La zone centrale de l'interface utilisateur Nextcloud. On y retrouve le contenu ou les fonctionnalités de l'application sélectionnée.

-   **La barre de navigation** (4) : Située sur la fenêtre d'affichage principale (vue de l'application), cette barre permet de naviguer entre les différents niveaux hiérarchiques des répertoires et de remonter jusqu'à la racine (représentée par la maison).

-   **Le bouton « Nouveau » ** (5) : Situé dans la barre de navigation, ce bouton vous permet de créer de nouveaux fichiers et dossiers ou d'envoyer des fichiers sur le serveur.



> **note**
>
> Vous pouvez également glisser/déposer des fichiers depuis votre explorateur de fichiers vers la vue de l'application Fichiers afin de les envoyer sur votre instance. À l'heure actuelle, seuls Chrome et Chromium permettent d'utiliser cette fonctionnalité.

-   **Le champ de recherche** (6) : Cliquez sur la loupe située dans le coin supérieur droit pour rechercher parmi les fichiers.


-   **Le menu des contacts** (7) : Ce menu fournit un aperçu des contacts et des utilisateurs sur votre serveur. Selon les informations et les applications disponibles, vous pouvez l'utiliser afin de démarrer un appel vidéo avec eux ou leur envoyer des messages.


-   **Le bouton de la galerie** (8). Ce bouton représenté par quatre petits carrés vous permet d'accéder à la galerie des images.


 -   **Le menu des paramètres personnels** menu (9) : Cliquer sur l'icône de la roue crantée ou sur votre image de profil, située à droite du champ de recherche afin d'accéder au menu déroulant des paramètres personnels. Votre page personnelle vous permet d'accéder aux fonctionnalités et aux réglages suivants :

  - Les liens pour télécharger les applications (bureau ou mobile
  - Lancer à nouveau l'assistant de première utilisation
  - L'espace serveur utilisé et l'espace disponible
  - La gestion du mot de passe
  - Les paramètres pour le nom, l'adresse électronique et l'image de profil
  - La gestion des navigateurs et des appareils connectés
  - Les groupes auxquels vous appartenez
  - Configuration de la langue de l'interface
  - La gestion des notifications
  - L'Identifiant Cloud fédéré et les boutons de partage sur les réseaux sociaux
  - La gestion des certificat SSL pour les stockages externes
  - Vos paramètres d'authentification en deux étapes
  - Les informations relatives à la version de Nextcloud

Consultez la section [Régler les préférences utilisateur](userpreferences.md) pour en savoir plus sur ces paramètres.
