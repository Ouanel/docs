# Prise en main

## Créer une discussion en 4 étapes

1. Choisissez le nom de vote salon ![nom du salon](images/framatalk01.png)
2. Autorisez l’utilisation de votre micro ![autorisation micro](images/framatalk02.png)
3. Autorisez l’utilisation de votre caméra ![autorisation caméra](images/framatalk03.png)
4. Partagez l’adresse web (URL) pour converser avec vos ami·e·s ![salon Framatalk](images/framatalk04-1.png)
