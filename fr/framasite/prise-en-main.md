# Prise en main

## Suppression d'un site

Pour supprimer votre site vous devez :

  * vous connecter sur [frama.site](https://frama.site/)
  * cliquer sur le bouton **Paramètres de ce site** dans le cadre du site que vous souhaitez modifier
  ![Suppresion de site](images/admin_suppression_site.png)
  * cliquer sur le bouton **Supprimer mon site** en bas de la page
  * entrer votre mot de passe **administrateur** (celui utilisé pour entrer sur [frama.site](https://frama.site/))
  * optionnel mais conseillé : vous pouvez aussi exporter votre site
  ![admin confirmation suppression](images/admin_confirmation_suppression.png)
  * cliquer sur **Confirmer la suppression du site**

## Suppression de votre compte

<div class="alert alert-danger">
    Les sites et wikis suivants associés à votre compte framasite seront <b>purement et simplement supprimés</b>, avec tous leurs contenus&nbsp;!
</div>

Pour supprimer votre compte vous devez :

  * vous connecter sur [frama.site](https://frama.site/)
  * cliquer sur le bouton **Mon compte**
  * cliquer sur l'onglet **Informations personnelles**
  * cliquer sur **Supprimer mon compte**
  ![admin suppression compte](images/admin_suppression_compte.png)
    * entrer votre mot de passe **administrateur** (celui utilisé pour entrer sur [frama.site](https://frama.site/))
    * cliquer sur **Supprimer mon compte**
